
Dropzone.autoDiscover = false; 
var seekersSupplierList = null;
var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();


var csrfSafeMethod = function(method) {
  // these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
};
$(function(){

   host = "http://"+window.location.hostname+":"+window.location.port
   console.log(host)
   classifierUri = host+"/"+ classifierUri
   // matcherUri = matcherUri
   console.log(classifierUri)
 // $("#mdzn").attr('action',classifierUri)
$.ajaxSetup({
      cache : false,
      beforeSend: function(xhr, settings) {
      csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });


Dropzone.autoDiscover = false; 

      loadGoogleMaps();

      var mdzn = new Dropzone("#mdzn",{
        url:classifierUri,
        headers: {'x-csrftoken':jQuery("[name=csrfmiddlewaretoken]").val()},
         // method:"post",  
        'success':function(fileuploaded, data){
          seekersSupplierList = data;
          matcherUri = data.matcher_url;
          delete seekersSupplierList['matcher_url']
      
          //console.log(fileuploaded)
          //console.log(seekersSupplierList)
          $("#upload-view").addClass('hidden');
          $("#file-view").removeClass('hidden');
          $("#title-file-view").removeClass('hidden');
          $("#from-view").removeClass('hidden');
          //console.log(seekersSupplierList)
          var options = {
            zoom:8,
            center:{lat:42.3601,lng:-71.0589}
          }
          map = new google.maps.Map(document.getElementById('map'), options);
          var infowindow = new google.maps.InfoWindow({maxWidth: 200});
          // list = {seekersSupplierList};
           list = seekersSupplierList;
           //console.log(typeof(list))
           var createMarker = function (position,content,iconImage) {
            var marker = new google.maps.Marker({
                position: position,
                content: content,
                icon: iconImage,
                map: map
          });
            google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(content);
            infowindow.open(map, marker);
            });
          }

          function twitterMarker(){ 
          for (var i = 0; i < list.length; i++) {            
            //console.log(list)
            var position = {lat:list[i].primary_latlng[0],lng:list[i].primary_latlng[1]}
            var content = 'ID-->' + list[i].tweet_id + '---TWEET-->' +  list[i].text
            var iconImage = null
            createMarker(position,content,iconImage);
            for (var j = 0; j < list[i].potential_offers.length; j++) {
              var position = {lat:list[i].potential_offers[j].primary_latlng[0],lng:list[i].potential_offers[j].primary_latlng[1]}
              var content = 'ID-->' + list[i].potential_offers[j].tweet_id + '---TWEET-->' +  list[i].potential_offers[j].text
              var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
              createMarker(position,content,iconImage);
            }
          }
        }
        function activityStream(){ 
          for (var i = 0; i < list.length; i++) {            
            //console.log(list)
            var position = {lat:list[i].object.location.latitude,lng:list[i].object.location.longitude}
            var content = 'RE_ID-->' + list[i].RE_ID + '---TWEET-->' +  list[i].object.content
            var iconImage = null
            createMarker(position,content,iconImage);
            for (var j = 0; j < list[i].potential_offers.length; j++) {
              var position = {lat:list[i].potential_offers[j].object.location.latitude,lng:list[i].object.location.longitude}
              var content = 'RE_ID-->' + list[i].potential_offers[j].RE_ID + '---TWEET-->' +  list[i].potential_offers[j].object.content
              var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
              createMarker(position,content,iconImage);
            }
          }
        }



        for (var i = 0; i < list.length; i++){
          
           if(list[i].RE_ID){
            activityStream();
           }
           if(list[i].tweet_id){
            twitterMarker();
           }
          }
          // $('#tree1').tree({data:list});
           $('#element').jsonViewer(seekersSupplierList,{collapsed:true});
           // $('#element').jsonViewer(JSON.stringify(seekersSupplierList, undefined, 2),{collapsed:true});
          // document.getElementById("disp").innerHTML = JSON.stringify(seekersSupplierList, undefined, 2);

            //  var createMarker = function (position,content,iconImage) {
            //       var marker = new google.maps.Marker({
            //           position: position,
            //           content: content,
            //           icon: iconImage,
            //           map: map
            //     });
      
              // google.maps.event.addListener(marker, 'click', function () {
              // infowindow.setContent(content);
              // infowindow.open(map, marker);
              // });
      
              //}
        }
       });
 
mdzn.options.url = classifierUri;


$("#upload").on('click',function(e){
  $("#upload-view").removeClass('hidden');
  $("#file-view").addClass('hidden');
  $("#title-file-view").addClass('hidden');
  $("#from-view").addClass('hidden');

});

   $("#match").on('click', function(e){
    // 
       var threshold = $("#threshold").val();
       var isFormValid =( threshold != null && threshold !='' &&  isNaN(threshold) === false && threshold <= 100 && threshold > 0);

       if(isFormValid === true){
      data = {}
      data.threshold =  threshold
      data.seekerslist = JSON.stringify(seekersSupplierList.seekers_list)
      data.supplierslist= JSON.stringify(seekersSupplierList.suppliers_list)

      //console.log(data)
      $.post( matcherUri,data).done( function( data ) {
       //console.log(data);
       list=data;
       var options = {
            zoom:8,
            center:{lat:42.3601,lng:-71.0589}
          }
      map = new google.maps.Map(document.getElementById('map'), options);
          var infowindow = new google.maps.InfoWindow({maxWidth: 200});
          // list = {seekersSupplierList};
           //list = seekersSupplierList;
           //console.log(typeof(list))
           var createMarker = function (position,content,iconImage) {
            var marker = new google.maps.Marker({
                position: position,
                content: content,
                icon: iconImage,
                map: map
          });
            google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(content);
            infowindow.open(map, marker);
            });
          }

          function twitterMarker(){ 
          for (var i = 0; i < list.length; i++) {            
            //console.log(list)
            var position = {lat:list[i].primary_latlng[0],lng:list[i].primary_latlng[1]}
            var content = 'ID-->' + list[i].tweet_id + '---TWEET-->' +  list[i].text
            var iconImage = null
            createMarker(position,content,iconImage);
            for (var j = 0; j < list[i].potential_offers.length; j++) {
              var position = {lat:list[i].potential_offers[j].primary_latlng[0],lng:list[i].potential_offers[j].primary_latlng[1]}
              var content = 'ID-->' + list[i].potential_offers[j].tweet_id + '---TWEET-->' +  list[i].potential_offers[j].text
              //var content = "<h1>"+content+"</h1>"
              var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
              createMarker(position,content,iconImage);
            }
          }
        }
        function activityStream(){ 
          for (var i = 0; i < list.length; i++) {            
            //console.log(list)
            var position = {lat:list[i].object.location.latitude,lng:list[i].object.location.longitude}
            var content = 'RE_ID-->' + list[i].RE_ID + '---TWEET-->' +  list[i].object.content
            var iconImage = null
            createMarker(position,content,iconImage);
            for (var j = 0; j < list[i].potential_offers.length; j++) {
              var position = {lat:list[i].potential_offers[j].object.location.latitude,lng:list[i].object.location.longitude}
              var content = 'RE_ID-->' + list[i].potential_offers[j].RE_ID + '---TWEET-->' +  list[i].potential_offers[j].object.content
              var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
              createMarker(position,content,iconImage);
            }
          }
        }


        //console.log('hello')
        //console.log(list)
        for (var i = 0; i < list.length; i++){
           if(list[i].RE_ID){
            activityStream();
           }
           if(list[i].tweet_id){
            twitterMarker();
           }
          }
      $('#element').jsonViewer(data,{collapsed:true});
      });
       
      }
      
       
    });
  // var infowindow = new google.maps.InfoWindow({maxWidth: 200});
});

 
$(document).ready(loadGoogleMaps)

var loadGoogleMaps = function () {
   
      
      if (document.querySelectorAll('#map').length > 0)
      {
        if (document.querySelector('html').lang)
          lang = document.querySelector('html').lang;
        else
          lang = 'en';

        var js_file = document.createElement('script');
        js_file.type = 'text/javascript';
        js_file.src = 'https://maps.googleapis.com/maps/api/js?key='+api_key+'&callback=initMap&language=' + lang;
        document.getElementsByTagName('head')[0].appendChild(js_file);
      }
}


var initMap = function () {

    var options = {
          zoom:8,
          center:{lat:42.3601,lng:-71.0589}
        }
        // New map
     map = new google.maps.Map(document.getElementById('map'), options);
    // var infowindow = new google.maps.InfoWindow({maxWidth: 200});
    // // list = {seekersSupplierList};
    //  list = list;
    
    // for (var i = 0; i < list.length; i++) {
    //   console.log(list)
    //   var position = {lat:list[i].primary_latlng[0],lng:list[i].primary_latlng[1]}
    //   var content = 'ID-->' + list[i].tweet_id + '---TWEET-->' +  list[i].text
    //   var iconImage = null
    //   createMarker(position,content,iconImage);
    //   for (var j = 0; j < list[i].potential_offers.length; j++) {
    //     var position = {lat:list[i].potential_offers[j].primary_latlng[0],lng:list[i].potential_offers[j].primary_latlng[1]}
    //     var content = 'ID-->' + list[i].potential_offers[j].tweet_id + '---TWEET-->' +  list[i].potential_offers[j].text
    //     var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
    //     createMarker(position,content,iconImage);
    //   }
    // }


    //    var createMarker = function (position,content,iconImage) {
    //         var marker = new google.maps.Marker({
    //             position: position,
    //             content: content,
    //             icon: iconImage,
    //             map: map
    //       });

    //     google.maps.event.addListener(marker, 'click', function () {
    //     infowindow.setContent(content);
    //     infowindow.open(map, marker);
    //     });

    //     }

   }