from sklearn.externals import joblib
from seeker_supplier.models import (request_model,request_model_CV,offer_model,offer_model_CV)
from ssc.settings import MEDIA_ROOT
import json
import os
import shutil

"""
Author - Kushal Mokashi

"""


dir_ = os.path.join(os.path.join(MEDIA_ROOT,"dumps"),"activity") 
"""
path to dump the files

"""

class Classifier():
    """
    To classify the seekers and suppliers

    """
    item = None
    seeker_data = None
    content = None
    seekerslist = []
    supplierslist = []
    otherslist = []
    data=None
    

    def __init__(self,item=None,content=None,seeker_data=None,data=None):
        self.item =item
        self.content= content
        self.seeker_data =seeker_data
        self.data = data
      
        # self.calculate_Seeker_Supplier(item,tweet,seeker_data)



    def classify(self):
        """
        Main logic to classify the seekers and suppliers

        """ 
        self.seekerslist = []
        self.supplierslist = []
        self.otherslist = []
        for item in self.data['items']:
            content = item['object']['content']
            seeker_data = {"RE_ID":None,"type":None,"object":{ "location":{}, "content":None}}
            self.calculate_Seeker_Supplier(item,content,seeker_data)
        self.dump_data_to_file()
        return self


    def calculate_Seeker_Supplier(self,item,content,seeker_data):
                """
                Calling mahine learning modules for classification

                """
                content_as_list=[content]
                seeker_data = self.get_locations_userinfo(item,seeker_data)
                content_cv= request_model_CV.transform(content_as_list)
                predictions = request_model.predict(content_cv)
                isAppended = self.append_seeker_item(predictions,seeker_data.copy())
                if  isAppended == False:
                    self.append_supplier_item(predictions,seeker_data.copy(),content_as_list)


          
    def dump_data_to_file(self,seekerslist=None,supplierslist=None,otherslist=None):
        """
        dumping the files to the machine  

        """
        if seekerslist: self.seekerslist = seekerslist
        if supplierslist: self.supplierslist = supplierslist
        if otherslist: self.otherslist = otherslist

        try:
            shutil.rmtree(os.path.join(dir_,'seekerslist.json'))
            os.remove(os.path.join(dir_,'seekerslist.json'))
        except Exception as e:
              print (e)

        try:
            shutil.rmtree(  os.path.join(dir_,'supplierslist.json'))
            os.remove( os.path.join(dir_,'supplierslist.json'))
        except Exception as e:
          print(e)

        try:
            shutil.rmtree(os.path.join(dir_,'otherslist.json'))
            os.remove(os.path.join(dir_,'otherslist.json'))
        except Exception as e:
              print (e) 

        with open(os.path.join(dir_,'seekerslist.json'), 'w') as fpot:
            json.dump(self.seekerslist, fpot, indent=4)
        with open(os.path.join(dir_,'supplierslist.json'), 'w') as fpsk:
            json.dump(self.supplierslist, fpsk, indent=4)
        with open(os.path.join(dir_,'otherslist.json'), 'w') as fpsp:
            json.dump(self.otherslist , fpsp, indent=4)


    def append_seeker_item(self,predictions,seeker_data):
        """
        appending the seekers list

        """
        if predictions == 1:
             seeker_data_new = seeker_data.copy()
             seeker_data_new['type'] = 'seeker'
             self.seekerslist.append(seeker_data_new)
             return True
        if predictions != 1:
            return False
 

    def append_supplier_item(self,predictions,seeker_data,content_as_list):
        """
        appending the suppliers list

        """
        content_cv_offer=offer_model_CV.transform(content_as_list)
        predictions_offer =offer_model.predict(content_cv_offer)
        if predictions_offer == 1:
            seeker_data_new = seeker_data.copy()
            seeker_data_new['type'] = 'supplier'
            self.supplierslist.append(seeker_data_new)
            return True
        if predictions_offer != 1:
            seeker_data_new = seeker_data.copy()
            seeker_data_new['type'] = 'others'
            self.append_others_item(seeker_data_new)
            return False


    def append_others_item(self,seeker_data):
        """
        appending the others list

        """
        self.otherslist.append(seeker_data.copy())
 
   
    def get_locations_userinfo(self,item,seeker_data):
        """
        fetching the location information to represent in map   

        """
        seeker_data['RE_ID'] = item['RE_ID']
        if 'location' in item['object'] :
            seeker_data['object']['location'] = item['object']['location']
        seeker_data['object']['content'] = item['object']['content']
        return seeker_data




    def get_data(self):
        return self.data
    
    def set_data(self,data):
        self.data=data

    def get_seekerslist(self):
        return self.seekerslist

    def set_seekerslist(self,seekerslist):
        self.seekerslist = seekerslist

    def get_otherslist(self):
            return self.otherslist

    def set_otherslist(self,otherslist):
        self.otherslist = otherslist

    def get_supplierslist(self):
            return self.supplierslist

    def set_supplierslist(self,supplierslist):
        self.supplierslist = supplierslist