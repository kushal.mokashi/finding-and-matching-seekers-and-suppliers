from sklearn.externals import joblib
from seeker_supplier.models import (request_model,request_model_CV,offer_model,offer_model_CV)
from ssc.settings import MEDIA_ROOT
import json
import os
import shutil

"""
Author - Kushal Mokashi

"""

#dir_ = r""+MEDIA_ROOT+"/dumps/twitter"
dir_=os.path.join(os.path.join(MEDIA_ROOT,"dumps"),"twitter")
"""
path to dump the files

"""
class Classifier():
    """
    To classify the seekers and suppliers

    """
    item = None
    seeker_data = None
    tweet = None
    seekerslist = []
    supplierslist = []
    otherslist = []
    data=None
    
    
    def __init__(self,item=None,tweet=None,seeker_data=None,data=None):
        self.item =item
        self.tweet= tweet
        self.seeker_data =seeker_data
        self.data = data
      
        # self.calculate_Seeker_Supplier(item,tweet,seeker_data)

    def classify(self):
        """
        Main logic to classify the seekers and suppliers

        """
        self.seekerslist = []
        self.supplierslist = []
        self.otherslist = []
        for item in self.data['items']:
            tweet = item['text']
            seeker_data = dict(tweet_id=None,text=None,type=None,features=dict(name=None,id=None,screen_name=None,primary_geo=[],geo_type=None))
            self.calculate_Seeker_Supplier(item,tweet,seeker_data)
        self.dump_data_to_file()
        return self

    def calculate_Seeker_Supplier(self,item,tweet,seeker_data):
                """
                Calling mahine learning modules for classification

                """
                tweetaslist=[tweet]
                seeker_data = self.get_locations_userinfo(item,seeker_data)
                tweetcv= request_model_CV.transform(tweetaslist)
                predictions = request_model.predict(tweetcv)
                isAppended = self.append_seeker_item(predictions,seeker_data.copy())
                if  isAppended == False:
                    self.append_supplier_item(predictions,seeker_data.copy(),tweetaslist)


            
    def dump_data_to_file(self,seekerslist=None,supplierslist=None,otherslist=None):
        """
        dumping the files to the machine  

        """
        if seekerslist: self.seekerslist = seekerslist
        if supplierslist: self.supplierslist = supplierslist
        if otherslist: self.otherslist = otherslist

        try:
            shutil.rmtree( r""+dir_+'/'+'seekerslist.json')
            os.remove( r""+dir_+'/'+'seekerslist.json')
        except Exception as e:
              print (e)

        try:
            shutil.rmtree( r""+dir_+'/'+'supplierslist.json')
            os.remove( r""+dir_+'/'+'supplierslist.json')
        except Exception as e:
          print(e)

        try:
            shutil.rmtree( r""+dir_+'/'+'otherslist.json')
            os.remove(dir_+'/'+'otherslist.json')
        except Exception as e:
              print (e) 

        with open(r''+dir_+'/'+'seekerslist.json', 'w') as fpot:
            json.dump(self.seekerslist, fpot, indent=4)
        with open(r''+dir_+'/'+'supplierslist.json', 'w') as fpsk:
            json.dump(self.supplierslist, fpsk, indent=4)
        with open(r''+dir_+'/'+'otherslist.json', 'w') as fpsp:
            json.dump(self.otherslist , fpsp, indent=4)


    def append_seeker_item(self,predictions,seeker_data):
        """
        appending the seekers list

        """
        if predictions == 1:
             seeker_data_new = seeker_data.copy()
             seeker_data_new['type'] = 'seeker'
             self.seekerslist.append(seeker_data_new)
             return True
        if predictions != 1:
            return False
 

    def append_supplier_item(self,predictions,seeker_data,tweetaslist):
        """
        appending the suppliers list

        """
        tweetcvoffer=offer_model_CV.transform(tweetaslist)
        predictions_offer =offer_model.predict(tweetcvoffer)
        if predictions_offer == 1:
            seeker_data_new = seeker_data.copy()
            seeker_data_new['type'] = 'supplier'
            self.supplierslist.append(seeker_data_new)
            return True
        if predictions_offer != 1:
            seeker_data_new = seeker_data.copy()
            seeker_data_new['type'] = 'other'
            self.append_others_item(seeker_data_new)
            return False

    def append_others_item(self,seeker_data):
        """
        appending the others list

        """
        self.otherslist.append(seeker_data.copy())
      
    def get_locations_userinfo(self,item,seeker_data):
        """
        fetching the location information to represent in map   

        """
        seeker_data['tweet_id'] = item['id_str']
        seeker_data['features']['name'] = item['user']['name']
        seeker_data['features']['id'] = item['user']['id']
        seeker_data['features']['screen_name'] = item['user']['screen_name']
        seeker_data['text'] = item['text']

        if item['coordinates'] is not None:
                        seeker_data["features"]["primary_geo"].append(item['coordinates']['coordinates'][1])
                        seeker_data["features"]["primary_geo"].append(item['coordinates']['coordinates'][0])
                        seeker_data["features"]["geo_type"] = "Tweet coordinates"
                        return seeker_data
        elif item['place'] is not None:
                        seeker_data["features"]["primary_geo"] = item['place']['full_name'] + ", " + item['place']['country']
                        seeker_data["features"]["geo_type"] = "Tweet place"
                        return seeker_data
        else:
                        seeker_data["features"]["primary_geo"] = item['user']['location']
                        seeker_data["features"]["geo_type"] = "User location"
                        return seeker_data
       




    def get_data(self):
        return self.data
    
    def set_data(self,data):
        self.data=data

    def get_seekerslist(self):
        return self.seekerslist

    def set_seekerslist(self,seekerslist):
        self.seekerslist = seekerslist

    def get_otherslist(self):
            return self.otherslist

    def set_otherslist(self,otherslist):
        self.otherslist = otherslist

    def get_supplierslist(self):
            return self.supplierslist

    def set_supplierslist(self,supplierslist):
        self.supplierslist = supplierslist