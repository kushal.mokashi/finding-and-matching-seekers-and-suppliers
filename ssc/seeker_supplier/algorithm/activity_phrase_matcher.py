import os,json
import gensim.models.word2vec as w2v
import pandas as pd
import math
import numpy as np
from nltk.corpus import stopwords
from .activity_phrase_vector import PhraseVector
from ssc.settings import MEDIA_ROOT
import shutil

"""
Author - Kushal Mokashi

"""

dir_ = os.path.join(os.path.join(MEDIA_ROOT,"dumps"),"activity")
"""
path to dump the files
"""


class PhraseMatcher():
    """
    Matching seekers and suppliers
    """
    tempdict = {"RE_ID":None,"type":None,"object":{ "location":{}, "content":None}}
    seekers_suppliers_list = []
    potential_list = []
    seekerslist_json = {}
    supplierslist_json = {}
    path_name = dir_
    threshold = 0.60
  
    def __init__(self, threshold=0.60,seekerslist_json={},supplierslist_json={}):
        self.threshold = threshold
        self.seekerslist_json = seekerslist_json
        self.supplierslist_json = supplierslist_json
        self.seekers_suppliers_list=[]

    def get_threshold(self):
        return self.threshold

    def set_threshold(self,threshold):
        self.threshold = threshold


    def set_seekers_suppliers_list(self):
        """
        Use Word2Vec and find the similarity between seekers and suppliers
        """
        i = 0
        for seeker in self.seekerslist_json:
            seekers_suppliers = {}
            seekers_suppliers['RE_ID'] = seeker['RE_ID']
            seekers_suppliers['type']= seeker['type']
            seekers_suppliers['object']= seeker['object']
            seekers_suppliers["potential_offers"] = []
            for supplier in self.supplierslist_json:
                phraseVector1 = PhraseVector(seeker['object']['content'])
                phraseVector2 = PhraseVector(supplier['object']['content']) 
                similarityScore  = phraseVector1.CosineSimilarity(phraseVector2.vector)
                similarityScore = float('%.2f' % (similarityScore * 100))
                if similarityScore >= self.threshold:
                    potential_offers = {"RE_ID":None,"type":None,"object":{ "location":{}, "content":None}}
                    potential_offers['RE_ID'] = supplier['RE_ID']
                    potential_offers['type'] = supplier['type']
                    potential_offers['object'] = supplier['object']
                    potential_offers['smiliarity_score_in_percentage'] = similarityScore
                    seekers_suppliers["potential_offers"].append(potential_offers)

            self.seekers_suppliers_list.append(seekers_suppliers.copy())
            i = i+1;    

    def get_seekers_suppliers_list(self):
        return self.seekers_suppliers_list

    def match(self, path_name=dir_):
        """
        Load the dumped files from the system
        """
        self.path_name = path_name
        self.seekers_suppliers_list = []
        if bool(self.seekerslist_json) == False:
            self.seekerslist_json = json.loads(open(r""+self.path_name+'/seekerslist.json').read())
        if bool(self.supplierslist_json) == False:
            self.supplierslist_json = json.loads(open(r""+self.path_name+'/supplierslist.json').read())
        self.set_seekers_suppliers_list()
        #print(self.seekers_suppliers_list)  
        try:
            shutil.rmtree(r""+self.path_name+'/matched/seekers_suppliers_list.json')
            os.remove(r""+self.path_name+'/matched/seekers_suppliers_list.json')
        except Exception as e:
            pass 
        with open(r""+self.path_name+'/matched/seekers_suppliers_list.json', 'w') as fpsk:
            json.dump(self.get_seekers_suppliers_list(), fpsk, indent=4)
        return self
