from django.apps import AppConfig


class SeekerSupplierConfig(AppConfig):
    name = 'seeker_supplier'
