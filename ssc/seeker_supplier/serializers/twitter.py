

class Twitter():
    id_str = None
    text = None
    coordinates = {}
    places = {}
    meta = {}
    rep_dict = {}
    vals_to_procces = {}

    def __init__(self,places=None,text=None,id_str=None,meta={},coordinates={}):
        self.places = places
        self.text = text
        self.id_str = id_str
        self.meta = meta
        self.coordinates = coordinates
        self.rep_dict = {'id_str':self.id_str, 'text':self.text,'coordinates':self.coordinates}
        self.vals_to_procces  = self.rep_dict
        for key,val in meta.items():
            if key not in self.rep_dict:
                self.rep_dict[key]=val

    def __init__(self,rep_dict={}):
        self.rep_dict = rep_dict
        self.places = self.rep_dict['places']
        self.text = self.rep_dict['text']
        self.id_str = self.rep_dict['id_str']
        self.coordinates = self.rep_dict['coordinates']
        self.vals_to_procces  =  {'id_str':self.id_str, 'text':self.text,'coordinates':self.coordinates}
        for key,val in rep_dict.items():
            if key not in self.avals_to_procces:
                self.meta[key]=val

    def get_vals_to_procces(self):
    	return self.vals_to_procces

    def set_vals_to_proccess(self,vals_to_procces):
    	self.vals_to_procces = vals_to_procces

    def get_rep_dict(self):
    	return self.rep_dict

    def set_rep_dict(self,rep_dict):
    	self.rep_dict = rep_dict

    def get_id_str(self):
    	return self.id_str
	
    def set_id_str(self,id_str):
    	self.id_str= id_str

    def get_text(self):
    	return self.text
		
    def set_text(self,text):
    	self.text = text


    def get_places(self):
    	return self.places
	
    def set_places(self,places):
    	self.places=places 


    def get_meta(self):
    	return  self.meta
	
    def set_meta(self,meta):
    	self.meta = meta


    def get_coodinates(self):
    	return self.coordinates
	
    def set_coordinates(self,coordinates):
    	self.coordinates = coordinates
