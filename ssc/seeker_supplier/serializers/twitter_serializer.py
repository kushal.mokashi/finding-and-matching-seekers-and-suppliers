from rest_framework import serializers
from .twitter import Twitter

class TwitterSerializer(serializers.Serializer):
    rep_dict = serializers.JSONField()


 
    def create(self, validated_data):
        return Twitter(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance