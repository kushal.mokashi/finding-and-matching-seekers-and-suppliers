

class ActivityStream():
    RE_ID = None
    Object = {'content':None, 'location':{}}
    rep_dict = {}
    vals_to_procces = {}
    meta = {}

    def __init__(self,RE_ID=None, Object={},meta={}):
        self.RE_ID = RE_ID
        self.Object = Object
        self.rep_dict = {'RE_ID':self.RE_ID, 'object':self.Object}
        self.vals_to_procces  = self.rep_dict
        for key,val in meta.items():
            if key not in self.rep_dict:
                self.rep_dict[key]=val
            if key == 'object':
                for k,v in v.items():
                    if k not in self.rep_dict['object']:
                        self.rep_dict[key]=v

    def __init__(self,rep_dict={}):
        self.rep_dict = rep_dict
        self.RE_ID = self.rep_dict['RE_ID']
        self.Object = self.rep_dict['object']
        self.vals_to_procces  =  {'RE_ID':self.RE_ID, 'object':self.Object}
        for key,val in rep_dict.items():
            self.meta[key]=val

    def get_vals_to_procces(self):
    	return self.vals_to_procces

    def set_vals_to_proccess(self,vals_to_procces):
    	self.vals_to_procces = vals_to_procces

    def get_rep_dict(self):
    	return self.rep_dict

    def set_rep_dict(self,rep_dict):
    	self.rep_dict = rep_dict

    def get_RE_ID(self):
    	return self.RE_ID
	
    def set_RE_ID(self,RE_ID):
    	self.RE_ID= RE_ID

    def get_Object(self):
    	return self.Object
		
    def set_Object(self,Object):
    	self.Object = Object

    def get_meta(self):
    	return  self.meta
	
    def set_meta(self,meta):
    	self.meta = meta

 