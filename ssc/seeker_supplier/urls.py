from django.urls import path,include,re_path as url

from . import views
from rest_framework.routers import DefaultRouter
from .api import twitter_classifier,activity_stream_classifier,twitter_matcher, activity_stream_matcher


twitter_router = DefaultRouter()
twitter_router.register('twitter-classifier', twitter_classifier.Classifier,base_name='twitter-classifier')
twitter_router.register('twitter-matcher', twitter_matcher.Matcher,base_name='twitter-matcher')

activity_stream_router = DefaultRouter()
activity_stream_router.register('activity-stream-classifier', activity_stream_classifier.Classifier,base_name='activity-stream-classifier')
activity_stream_router.register('activity-stream-matcher', activity_stream_matcher.Matcher, base_name='activity-stream-matcher')

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^stream_type/(?P<stream_type>[^/]+)/$', views.index, name='index'),
    url(r'^api/twitter/',include(twitter_router.urls), name="twitter"),
    url(r'^api/activity-stream/',include(activity_stream_router.urls) ,name="activity")
 
]
