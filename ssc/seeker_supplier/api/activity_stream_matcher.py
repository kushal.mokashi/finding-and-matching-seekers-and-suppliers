from rest_framework.response import Response
from rest_framework.authentication import (BasicAuthentication, SessionAuthentication,TokenAuthentication)
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import (IsAuthenticatedOrReadOnly, IsAuthenticated)
from rest_framework import (viewsets, generics, status, filters)
from seeker_supplier.serializers.activity_stream_serializer import ActivityStreamSerializer
from seeker_supplier.algorithm.activity_phrase_matcher  import PhraseMatcher as PSM
import json
from pprint import pprint

"""
Author - Kushal Mokashi

"""


class Matcher(viewsets.ViewSet):
    """
    Receiving the matcher API call for activity stream file

    """
   
    # user = None
    serializer_class = ActivityStreamSerializer
    # pagination_class = None
    # search_fields = ( )
    # filter_backends = ( )
    # authentication_classes = ()
    # (BasicAuthentication, SessionAuthentication,TokenAuthentication)
    # permission_classes = ()

    def get_queryset(self):
        return qset_list

    def list(self, request):
        return Response("LIST", status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk=None):
        return Response("RETRIVE", status=status.HTTP_204_NO_CONTENT)
  

    def create(self, request):
        """
        HTTP POST request handler
    
        """
        data  = {}
        gc = None
        psm = None
        for k,v in request.data.items():
                data[k] = json.loads(v)
 
        psm = PSM(threshold=float(data['threshold']),seekerslist_json=data['seekerslist'],supplierslist_json=data['supplierslist']).match()
        seekers_suppliers_list=psm.get_seekers_suppliers_list()
        # pprint(seekers_suppliers_list)
        # gc = GC(seekers_suppliers_list=seekers_suppliers_list).geocode()
        # response = (gc is None == True)  and {} or gc
        response = (seekers_suppliers_list is None == True)  and {} or seekers_suppliers_list
        return Response(response, status=status.HTTP_200_OK)


    def put(self, request, pk=None):
         
        return Response("PUT", status=status.HTTP_200_OK)

    def partial_update(self,request, pk=None):
        return Response("PUT PARTIAL", status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        return Response("destroy", status=status.HTTP_200_OK)
 