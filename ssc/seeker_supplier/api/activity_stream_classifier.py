from seeker_supplier.serializers.activity_stream_serializer import ActivityStreamSerializer
from rest_framework.response import Response
from rest_framework.authentication import (BasicAuthentication, SessionAuthentication,TokenAuthentication)
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import (IsAuthenticatedOrReadOnly, IsAuthenticated,AllowAny)
from rest_framework import (viewsets, generics, status, filters)
from seeker_supplier.serializers.twitter_serializer import TwitterSerializer
from seeker_supplier.algorithm.twitter_classifier  import Classifier as TCSF
from seeker_supplier.algorithm.twitter_phrase_matcher  import PhraseMatcher as TPSM
from seeker_supplier.algorithm.activity_classifier  import Classifier as ACSF
from seeker_supplier.algorithm.activity_phrase_matcher  import PhraseMatcher as APSM
from django.core.files.storage import FileSystemStorage
from django.urls import reverse
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse,HttpResponseNotAllowed
from ssc import settings
import uuid
import os
import json
import shutil

"""
Author - Kushal Mokashi

"""


class Classifier(viewsets.ViewSet):
    """
    Receiving the classifier API call for activity stream filei

    """
    serializer_class = ActivityStreamSerializer
   
    def get_queryset(self):
        return qset_list

    def list(self, request):
        return Response("LIST", status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk=None):
        return Response("RETRIVE", status=status.HTTP_204_NO_CONTENT)


    def create(self, request):
        """
        HTTP POST request handler

        """
        response = {}
        if request.FILES['file']:
            dir_ = settings.MEDIA_ROOT+"/uploads"
            file = request.FILES['file']
            loaded=file.read().decode('utf-8')
            data = json.loads(loaded)
            if ('RE_ID' not in data['items'][0]) == True:
                cs  = TCSF(data=data).classify()
                response['seekers_list']=cs.seekerslist
                response['suppliers_list']=cs.supplierslist
                response['matcher_url']= '/api/twitter/twitter-matcher/'
                return Response(response, status=status.HTTP_200_OK)

            if ('RE_ID' not in data['items'][0]) == False:
                cs  = ACSF(data=data).classify()
                response['seekers_list']=cs.seekerslist
                response['suppliers_list']=cs.supplierslist
                response['matcher_url']= '/api/activity-stream/activity-stream-matcher/'
                return Response(response, status=status.HTTP_200_OK)
       
        else:
            return Response(response, status=status.HTTP_200_OK)

    def put(self, request, pk=None):
         
        return Response("PUT", status=status.HTTP_200_OK)

    def partial_update(self,request, pk=None):
        return Response("PUT PARTIAL", status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        return Response("destroy", status=status.HTTP_200_OK)
 