from ssc import settings
import os
from sklearn.externals import joblib

BASE = os.path.join(settings.BASE_DIR,'seeker_supplier/models/')
request_model = joblib.load(os.path.join(BASE,'random_forest_request_other'))
request_model_CV = joblib.load(os.path.join(BASE,'random_forest_request_other_tfidf')) 
offer_model = joblib.load(os.path.join(BASE,'random_forest_offer_other')) 
offer_model_CV = joblib.load(os.path.join(BASE,'random_forest_offer_other_tfidf')) 
word_to_vector_sandy = joblib.load(os.path.join(BASE,'wordtovecdatasandy.w2v')) 
__all__ = [
'request_model' , 
'request_model_CV', 
'offer_model',
'offer_model_CV',
'word_to_vector_sandy'
 ]