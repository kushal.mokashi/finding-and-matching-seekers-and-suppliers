# Seekers and Suppliers Classifier and Matcher

This application accepts two kinds of files. Twitter stream json file and Activity stream json file. Once you upload the file you will get an output as two lists 'seekers' and 'suppliers'. Provide a threshold for matching 'seekers' and 'suppliers'. The threshold should be between 0 and 100. You will receive matched list of 'seekers' and 'suppliers' as an output. Also the locations of the matched 'seekers' and 'suppliers' are marked in the map, which will be visible on the screen.This application is built using machine learning algorithms.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

1. Pull the repository to your local machine
2. Open the command prompt and change to the project folder[API-Matching]
3. For installing all the required libraries and prerequisites run the following command
	pip install -r requirements.txt
				OR
	Just run the environment i.e. 
	--cd env
	--cd scripts
	--activate
4. Final step to start the application run the following commands
	--cd API-Matching/ssc
	--python manage.py runserver
5. There is a folder named "Sample_Input_files_for_the_application", which contains the sample files to test the application.

### Prerequisites

What things you need to install the software and how to install them

```
The requirements.txt will take care of all the required libraries
```

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used.
* [Machine Learning Library](http://scikit-learn.org/stable/) - Machine Learning framework and libraries uesd.

## Code to generate Machine Learning models

The folder named "Generate_Machine_learning_Models" contains all the codes to generate the models.
Each file is named with respect to its used algorithm and a vectorizer.
The files are .ipynb extension and can be opened through Jupyter notebook. (http://jupyter.org/) 
It also contains a folder named "Input_file_used_for_generating_the_models", which has the data input files for the model.

## Author

* **Kushal Mokashi**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

